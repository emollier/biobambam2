/**
    biobambam2
    Copyright (C) 2009-2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(REFERENCE_HPP)
#define REFERENCE_HPP

#include <libmaus2/fastx/FastAReader.hpp>
#include <libmaus2/bambam/BamHeader.hpp>

struct Reference
{
	typedef Reference this_type;
	typedef std::unique_ptr<this_type> unique_ptr_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::vector<std::string> Vtext;
	std::vector<std::string> Vnames;
	std::vector< std::pair<std::string,uint64_t> > Vnameid;
	std::vector<uint64_t> Vmap;

	struct NameIdComparator
	{
		bool operator()(std::pair<std::string,uint64_t> const & A, std::pair<std::string,uint64_t> const & B) const
		{
			return A.first < B.first;
		}
	};

	Reference(std::string const & faname)
	{
		libmaus2::fastx::FastAReader FARe(faname);
		libmaus2::fastx::FastAReader::pattern_type pattern;

		for ( uint64_t uid = 0; FARe.getNextPatternUnlocked(pattern); ++uid )
		{
			std::string const sid = pattern.getShortStringId();
			std::string const & spattern = pattern.spattern;

			Vtext.push_back(spattern);
			Vnames.push_back(sid);
			Vnameid.push_back(
				std::pair<std::string,uint64_t>(
					sid,uid
				)
			);

			std::cerr << "[R][" << uid << "]=" << sid << std::endl;
		}

		std::sort(Vnameid.begin(),Vnameid.end());

		for ( uint64_t i = 1; i < Vnameid.size(); ++i )
			if ( !(Vnameid[i-1].first < Vnameid[i].first) )
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] short name " << Vnameid[i].first << " appears multiple times in " << faname << std::endl;
				lme.finish();
				throw lme;
			}
	}

	uint64_t getIdFor(std::string const & s) const
	{
		typedef std::vector< std::pair<std::string,uint64_t> >::const_iterator it;

		std::pair<it,it> const P = std::equal_range(
			Vnameid.begin(),
			Vnameid.end(),
			std::pair<std::string,uint64_t>(s,0),
			NameIdComparator()
		);

		if ( P.first != P.second )
			return P.first->second;
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] sequence name " << s << " is not contained in reference" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void computeMap(libmaus2::bambam::BamHeader const & header)
	{
		for ( uint64_t i = 0; i < header.getNumRef(); ++i )
		{
			char const * name = header.getRefIDName(i);
			uint64_t const id = getIdFor(std::string(name));
			Vmap.push_back(id);

			// std::cerr << "[M] " << i << " -> " << id << std::endl;
		}
	}

	std::string const & getSequence(uint64_t const i) const
	{
		if ( i < Vmap.size() )
		{
			return Vtext[Vmap[i]];
		}
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] sequence id " << i << " is not contained in reference" << std::endl;
			lme.finish();
			throw lme;
		}
	}
};
#endif
