/**
    biobambam
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <biobambam2/BamBamConfig.hpp>
#include <biobambam2/Licensing.hpp>
#include <biobambam2/DepthInterval.hpp>
#include <biobambam2/DepthIntervalGetter.hpp>

#include <libmaus2/util/ArgParser.hpp>

#include <iomanip>

#include <config.h>

void bamdepthmerge(libmaus2::util::ArgParser const & arg)
{
	biobambam2::DepthIntervalGetterMerge DIGM(arg.restargs);

	uint64_t refid;
	uint64_t p;
	libmaus2::autoarray::AutoArray< std::pair<uint64_t,uint64_t> > A;
	uint64_t o = 0;

	for ( uint64_t tupleid = 0; (o=DIGM.getNext(refid,p,A)); ++tupleid )
	{
		bool const print = (tupleid%(64*1024))==0;

		if ( print )
		{
			std::cout << refid << "\t" << p << "\t" << o;

			for ( uint64_t i = 0; i < o; ++i )
				std::cout << "\t(" << A[i].first << "," << A[i].second << ")";

			std::cout << "\n";
		}
	}
}

int main(int argc, char * argv[])
{
	try
	{
		std::vector<libmaus2::util::ArgParser::ArgumentDefinition> Vformat;
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("","mindepth",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("","maxdepth",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("","binary",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("v","verbose",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("T","",true));

		::libmaus2::util::ArgParser arg(argc,argv,Vformat);

		bamdepthmerge(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
