/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>

#include <libmaus2/vcf/VCFParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/lz/BgzfDeflate.hpp>

#include <biobambam2/Licensing.hpp>

bool getDefaultVerbose() { return true; }

int vcffilterinfox(libmaus2::util::ArgInfo const & arginfo, std::istream & in, std::ostream & out)
{
	//unsigned int const verbose = arginfo.getValue<unsigned int>("verbose",getDefaultVerbose());
	std::string const filter = arginfo.getUnparsedValue("filter",std::string());
	bool const gz = arginfo.getValue<int>("gz",0);

	if ( gz )
	{
		libmaus2::lz::BgzfOutputStream BOS(out);
		libmaus2::vcf::VCFParser::runFilterSamples(in,BOS,filter);
		BOS.flush();
		BOS.addEOFBlock();
	}
	else
	{
		libmaus2::vcf::VCFParser::runFilterSamples(in,out,filter);
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "verbose=<["+::biobambam2::Licensing::formatNumber(getDefaultVerbose())+"]>", "print progress report (default: 1)" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return vcffilterinfox(arginfo,std::cin,std::cout);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
