/*
    biobambam2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/bambam/BamMultiAlignmentDecoderFactory.hpp>
#include <libmaus2/bambam/BamBlockWriterBaseFactory.hpp>

#include <biobambam2/BamBamConfig.hpp>
#include <biobambam2/Licensing.hpp>

static int getDefaultLevel() {return libmaus2::lz::DeflateDefaults::getDefaultLevel();}
int getDefaultVerbose() { return 0; }
std::string getDefaultInputFormat() { return "bam"; }
uint64_t getDefaultSizeThres() { return 32*1024*1024; }
std::string getDefaultPrefix() { return "split_"; }

int bamexploderef(libmaus2::util::ArgInfo const & arginfo)
{
	std::string const inputformat = arginfo.getUnparsedValue("inputformat", libmaus2::bambam::BamAlignmentDecoderInfo::getDefaultInputFormat());
	std::string const reference = arginfo.getUnparsedValue("reference", libmaus2::bambam::BamAlignmentDecoderInfo::getDefaultReference());
	uint64_t const inputthreads = 1;
	std::string const outputformat = arginfo.getUnparsedValue("outputformat",libmaus2::bambam::BamBlockWriterBaseFactory::getDefaultOutputFormat());
	std::string const prefix = arginfo.getUnparsedValue("prefix",getDefaultPrefix());
	uint64_t const threads = arginfo.getValue<uint64_t>("threads",1);

	for ( uint64_t i = 0; i < arginfo.getNumRestArgs(); ++i )
	{
		std::string const infn = arginfo.getUnparsedRestArg(i);
		std::vector<std::string> Vref;

		{
			libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type Preader(
				libmaus2::bambam::BamAlignmentDecoderFactory::construct(std::cin,infn,inputformat)
			);
			libmaus2::bambam::BamHeader const & header = Preader->getDecoder().getHeader();
			for ( uint64_t j = 0; j < header.getNumRef(); ++j )
			{
				std::string const refname = header.getRefIDName(j);
				Vref.push_back(refname);
			}
		}

		std::vector<std::string> Vout(Vref.size());
		std::atomic<int> gfailed(0);
		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
		#endif
		for ( uint64_t j = 1; j < Vref.size(); ++j )
		{
			try
			{
				// std::cerr << "[V] processing ref " << Vref[j] << std::endl;

				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type Preader(
					libmaus2::bambam::BamAlignmentDecoderFactory::construct(std::cin,infn,inputformat,inputthreads,reference,false /* putrank */,NULL /* copystr */,Vref[j] /* range */)
				);

				libmaus2::bambam::BamAlignmentDecoder & decoder = Preader->getDecoder();
				libmaus2::bambam::BamHeader const & header = decoder.getHeader();
				libmaus2::bambam::BamAlignment const & algn = decoder.getAlignment();

				std::ostringstream fnostr;
				fnostr
					<< prefix
					<< std::setw(6) << std::setfill('0') << i << std::setw(0)
					<< std::setw(6) << std::setfill('0') << j << std::setw(0)
					<< std::setw(0) << "." << outputformat
					;

				std::string const fn = fnostr.str();
				libmaus2::util::ArgInfo argcopy = arginfo;
				argcopy.replaceKey("O",fn);
				libmaus2::bambam::BamBlockWriterBase::unique_ptr_type Pwriter(libmaus2::bambam::BamBlockWriterBaseFactory::construct(header,argcopy));
				Vout[j] = fn;


				while ( decoder.readAlignment() )
					Pwriter->writeAlignment(algn);

				Pwriter.reset();
			}
			catch(std::exception const & ex)
			{
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << "[E] error for sequence " << j << " (" << Vref[j] << "):" << std::endl;
				std::cerr << ex.what() << std::endl;
				gfailed = 1;
			}
		}

		if ( gfailed )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] bamexploderef failed in parallel loop" << std::endl;
			lme.finish();
			throw lme;
		}

		for ( uint64_t j = 0; j < Vout.size(); ++j )
		{
			std::cout << infn << "\t" << j << "\t" << Vout[j] << "\n";
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "level=<["+::biobambam2::Licensing::formatNumber(getDefaultLevel())+"]>", libmaus2::bambam::BamBlockWriterBaseFactory::getBamOutputLevelHelpText() ) );
				V.push_back ( std::pair<std::string,std::string> ( "verbose=<["+::biobambam2::Licensing::formatNumber(getDefaultVerbose())+"]>", "print progress report" ) );
				V.push_back ( std::pair<std::string,std::string> ( std::string("inputformat=<[")+getDefaultInputFormat()+"]>", std::string("input format (") + libmaus2::bambam::BamMultiAlignmentDecoderFactory::getValidInputFormats() + ")" ) );
				V.push_back ( std::pair<std::string,std::string> ( std::string("outputformat=<[")+libmaus2::bambam::BamBlockWriterBaseFactory::getDefaultOutputFormat()+"]>", std::string("output format (") + libmaus2::bambam::BamBlockWriterBaseFactory::getValidOutputFormats() + ")" ) );
				V.push_back ( std::pair<std::string,std::string> ( "I=<[stdin]>", "input filename (standard input if unset)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "inputthreads=<[1]>", "input helper threads (for inputformat=bam only, default: 1)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "reference=<>", "reference FastA (.fai file required, for cram i/o only)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "range=<>", "coordinate range to be processed (for coordinate sorted indexed BAM input only)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "outputthreads=<[1]>", "output helper threads (for outputformat=bam only, default: 1)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "O=<[stdout]>", "output filename (standard output if unset)" ) );
				V.push_back ( std::pair<std::string,std::string> ( std::string("prefix=<[")+getDefaultPrefix()+"]>", "prefix of output file names" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return bamexploderef(arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
