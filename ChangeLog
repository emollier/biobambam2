biobambam2 (2.0.178-1) unstable; urgency=medium

  * Add bamaddne
  * bump libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 23 Nov 2020 14:39:27 +0100

biobambam2 (2.0.177-1) unstable; urgency=medium

  * Versioning cleanup

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 12 Nov 2020 10:39:59 +0100

biobambam2 (2.0.176-1) unstable; urgency=medium

  * Adapt UpdateNumericalIndex to new libmaus2 api
  * Fix output of orphaned read 2 instances in bamtofastq when splitting by readgroup
  * Remove references to non functional IRODS interface in libmaus2
  * Add bamdifference program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 12 Nov 2020 09:43:13 +0100

biobambam2 (2.0.175-1) unstable; urgency=medium

  * Add fastaselectreg

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 27 Aug 2020 10:08:12 +0200

biobambam2 (2.0.174-1) unstable; urgency=medium

  * Fix wrong operator for deallocating memory in bamconsensus

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 10 Aug 2020 11:26:08 +0200

biobambam2 (2.0.173-1) unstable; urgency=medium

  * Fix for libmaus2 update

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 05 Aug 2020 14:41:16 +0200

biobambam2 (2.0.172-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 30 Jul 2020 09:10:25 +0200

biobambam2 (2.0.171-1) unstable; urgency=medium

  * replace UNIQUE_PTR_MOVE by std::move

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 21 Jul 2020 17:47:31 +0200

biobambam2 (2.0.170-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 20 Jul 2020 13:34:09 +0200

biobambam2 (2.0.169-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 17 Jul 2020 11:47:30 +0200

biobambam2 (2.0.168-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 07 Jul 2020 17:00:28 +0200

biobambam2 (2.0.167-1) unstable; urgency=medium

  * bump decompress buffer size when necessary in bamauxmerge2

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 19 Jun 2020 12:49:56 +0200

biobambam2 (2.0.166-1) unstable; urgency=medium

  * make gz non value argument in vcfreplacecontigsmap

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 09 Jun 2020 13:21:54 +0200

biobambam2 (2.0.165-1) unstable; urgency=medium

  * Add vcfreplacecontigsmap program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 08 Jun 2020 16:56:00 +0200

biobambam2 (2.0.164-1) unstable; urgency=medium

  * Add vcffilterfilterflags

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 07 May 2020 13:45:25 +0200

biobambam2 (2.0.163-1) unstable; urgency=medium

  * do not use assert in parallel loop of bamfilterk

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 22 Apr 2020 09:13:28 +0200

biobambam2 (2.0.162-1) unstable; urgency=medium

  * Add mapped/unmapped in bamcountflags

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 30 Mar 2020 15:11:34 +0200

biobambam2 (2.0.161-1) unstable; urgency=medium

  * Add bamcountflags

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 30 Mar 2020 14:31:52 +0200

biobambam2 (2.0.160-1) unstable; urgency=medium

  * Add bamdepthmerge for merging several depth tracks into a single table
  * Add merging framework in DepthIntervalGetter.hpp
  * Explicitely init DepthInterval in constructor
  * change/fix loop bounds in bamfeaturecount
  * Check for exception in parallel loop of bamexondepth

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 25 Mar 2020 18:00:57 +0100

biobambam2 (2.0.159-1) unstable; urgency=medium

  * Add cram compression mode option in bamsormadup
  * refactor bamexondepth
  * bump libmaus2 version
  * add gc content based analysis in bamexondepth
  * parallelise bamexondepth
  * change serialised form of DepthInterval and add comparator classes
  * fix warning in bamadapterfind

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 16 Mar 2020 13:35:22 +0100

biobambam2 (2.0.158-1) unstable; urgency=medium

  * bump libmaus2 version
  * Output coverage information in bamexondepth
  * Fix default depth setting in bamdepth
  * add avl tree tracking in bamexondepth
  * fix warnings

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 09 Mar 2020 09:02:03 +0100

biobambam2 (2.0.157-1) unstable; urgency=medium

  * Adapt bamexondepth from new code for bamdepth
  * bump libmaus2 version
  * Refactor bamdepth
  * Add program bamsimpledepth
  * Fix bamdepth (do not output multiple adjacent intervals for same depth)
  * update ignore file
  * Add vcfdiff program
  * Print complete alignment in case of exception in bamfeaturecount
  * Add bamexondepth and bamheadercat programs
  * add bammarkduplicatesoptdist
  * fix warnings
  * add print method in DepthInterval class
  * be more verbose in fastqtobam2 inflate failure case

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 04 Mar 2020 09:12:21 +0100

biobambam2 (2.0.156-1) unstable; urgency=medium

  * Add program bamexploderg for splitting an alignment file by read groups

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 24 Feb 2020 08:08:51 +0100

biobambam2 (2.0.155-1) unstable; urgency=medium

  * adapt cigar string on the fly in bamconsensus

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 21 Feb 2020 13:13:11 +0100

biobambam2 (2.0.154-1) unstable; urgency=medium

  * add bamfilterbyname and vcffiltersamples programs

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 18 Feb 2020 12:45:33 +0100

biobambam2 (2.0.153-1) unstable; urgency=medium

  * check for failure of parallel loop in bamfeaturecount and change sort buffer size

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 24 Jan 2020 12:29:53 +0100

biobambam2 (2.0.152-1) unstable; urgency=medium

  * update libmaus2 version to 2.0.694

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 22 Jan 2020 09:58:27 +0100

biobambam2 (2.0.151-1) unstable; urgency=medium

  * bump libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 21 Jan 2020 16:41:38 +0100

biobambam2 (2.0.150-1) unstable; urgency=medium

  * Add cols (line wrapping) option in bamtofastq

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 07 Jan 2020 16:37:02 +0100

biobambam2 (2.0.149-1) unstable; urgency=medium

  * use kmerCallbackPosFR instead of kmerCallbackPos in bamfilterk
  * Add vcfreplacecontigs program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 07 Jan 2020 09:02:29 +0100

biobambam2 (2.0.148-1) unstable; urgency=medium

  * compute maximum depth in bamfeaturecount
  * fix output of wrong reference id in bamfeaturecount if GTF chrom set is a subset of BAM chrom set

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 13 Nov 2019 12:53:00 +0100

biobambam2 (2.0.147-1) unstable; urgency=medium

  * fix heap comparisons in bamconsensus

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 05 Nov 2019 10:19:07 +0100

biobambam2 (2.0.146-1) unstable; urgency=medium

  * add explicit flush in bamtofastq
  * bump libmaus2 version
  * explicitely call flush on BamToFastqOutputFileSet objects in bamtofastq (try to avoid triggering exceptions in object destructor)
  * allow disabling adapter detection/clipping in fastqtobam2
  * use dynamic scheduling in bamfilterk

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 30 Oct 2019 10:48:48 +0100

biobambam2 (2.0.145-1) unstable; urgency=medium

  * refactor vcfsort
  * add kmer end position on reference in bamconsensus
  * fixes after libmaus2 changes

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 11 Oct 2019 12:46:33 +0200

biobambam2 (2.0.144-1) unstable; urgency=medium

  * update list of uncommon programs

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 27 Sep 2019 13:37:44 +0200

biobambam2 (2.0.143-1) unstable; urgency=medium

  * fix newline issue in Makefile

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 27 Sep 2019 13:11:19 +0200

biobambam2 (2.0.142-1) unstable; urgency=medium

  * add install-uncommon configure option to deselect some programs from standard installation

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 27 Sep 2019 12:58:23 +0200

biobambam2 (2.0.141-1) unstable; urgency=medium

  * bump libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 27 Sep 2019 11:24:14 +0200

biobambam2 (2.0.140-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 27 Sep 2019 08:57:03 +0200

biobambam2 (2.0.139-1) unstable; urgency=medium

  * add bamheap3 program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 19 Sep 2019 14:09:13 +0200

biobambam2 (2.0.138-1) unstable; urgency=medium

  * add exportcdna option in bamfeaturecount

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 28 Aug 2019 15:50:12 +0200

biobambam2 (2.0.137-1) unstable; urgency=medium

  * add manual page for bamfeaturecount
  * add manual page for filtergtf
  * add filtergtf program
  * implement simplified transcript level analysis in bamfeaturecount

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 28 Aug 2019 10:04:09 +0200

biobambam2 (2.0.136-1) unstable; urgency=medium

  * add bamfixpairinfo program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 22 Aug 2019 14:44:39 +0200

biobambam2 (2.0.135-1) unstable; urgency=medium

  * add UniqueInterval class in bamfeaturecount
  * add readgroup and program header lines in bamconsensus
  * fix border check in bamrecalculatecigar
  * fix computation of MD/NM fields in bamconsensus
  * make exclude flags user settable in bamconsensus
  * change default verbose setting in bamfillquery
  * update synopsis in bamconsensus help message
  * make bamconsensus output BAM file containing alignments of consensus with the reference instead of just the consensus as FastA
  * construct fasta index in bamrecalculatecigar if needed
  * add unmapped option for bamfilterlength
  * add bamconsensus to list of installed programs and add documentation
  * improve scaling in bamconsensus
  * make running dot optional in bamconsensus

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 22 Aug 2019 10:47:00 +0200

biobambam2 (2.0.134-1) unstable; urgency=medium

  * bump libmaus2 version
  * be more verbose in bamrecalculatecigar when the input is not suitably sorted
  *add fragmergepar option in bamsormadup to allow (some) control concerning the number of simultaneously open files used in duplicate marking

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 19 Aug 2019 15:18:51 +0200

biobambam2 (2.0.133-1) unstable; urgency=medium

  * update libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 16 Aug 2019 13:32:26 +0200

biobambam2 (2.0.132-1) unstable; urgency=medium

  * use heaviest path per connected component as representative in bamconsensus

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 16 Aug 2019 13:14:32 +0200

biobambam2 (2.0.131-1) unstable; urgency=medium

  * update help message in bamfilterk
  * add bamfilterk

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 13 Aug 2019 09:36:30 +0200

biobambam2 (2.0.130-1) unstable; urgency=medium

  * support numerical indexing in fastqtobam2

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 08 Aug 2019 11:13:12 +0200

biobambam2 (2.0.129-1) unstable; urgency=medium

  * rename fastaexplode to fastaexplod

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 08 Aug 2019 09:29:48 +0200

biobambam2 (2.0.128-1) unstable; urgency=medium

  * remove manual page for removed kmerprob
  * remove bamrefdepth and bamrefdepthpeaks

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 31 Jul 2019 14:10:27 +0200

biobambam2 (2.0.127-1) unstable; urgency=medium

  * add bamdepthintersect program
  * add bamdepth program
  * add DepthInterval class
  * remove hard coded Z_DEFAULT_COMPRESSION default and replace by DeflateDefaults::getDefaultLevel()

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 31 Jul 2019 12:53:58 +0200

biobambam2 (2.0.126-1) unstable; urgency=medium

  * allow output to standard output in bamnumericalindex

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 30 Jul 2019 11:11:47 +0200

biobambam2 (2.0.125-1) unstable; urgency=medium

  * add construction of numerical index from numerical indexes of input files in bamfastcat

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 26 Jul 2019 17:17:43 +0200

biobambam2 (2.0.124-1) unstable; urgency=medium

  * update numerical index in bamreplacechecksums
  * update numerical index in bamreheader
  * put header and data in separate blocks in ReadHeader

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 25 Jul 2019 15:29:48 +0200

biobambam2 (2.0.123-1) unstable; urgency=medium

  * avoid explicitely invoking BamDecoder class (use factory instead)
  * use output factory to allow SAM/BAM/CRAM output in bam12auxmerge
  * use input factories in bam12auxmerge to support SAM and CRAM input
  * add manual page for bamclipXT
  * remove filtersam and kmerprob

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 25 Jul 2019 11:10:33 +0200

biobambam2 (2.0.122-1) unstable; urgency=medium

  * add bamnumericalindexstats

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 24 Jul 2019 15:01:14 +0200

biobambam2 (2.0.121-1) unstable; urgency=medium

  * add numerical indexing in bamsormadup

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 24 Jul 2019 14:34:22 +0200

biobambam2 (2.0.120-1) unstable; urgency=medium

  * add vcfsort program
  * remove bamclipextract
  * simplify bamdisthist

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 24 Jul 2019 11:02:17 +0200

biobambam2 (2.0.119-1) unstable; urgency=medium

  * add manual page for bambisect
  * fix manual page for bamchecksort
  * update help message in bamauxmerge2
  * remove bamcheckalignments
  * add manual page for bamauxmerge2
  * add manual page for bamauxmerge
  * clean up bamauxmerge
  * change input order in bamalignfrac
  * add documentation for regex option in manual page for bamalignfrac
  * remove bamalignmentoffsets program
  * add manual page for bamalignfrac
  * rewrite bamalignfrac to produce more useful output

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 23 Jul 2019 21:55:28 +0200

biobambam2 (2.0.118-1) unstable; urgency=medium

  * avoid writing empty BGZF blocks in ReadHeader
  * remove bamlastfilter
  * remove deprecated bamcollate program

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 22 Jul 2019 09:36:51 +0200

biobambam2 (2.0.117-1) unstable; urgency=medium

  * fix breakages after libmaus2 cleanup

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 19 Jul 2019 13:09:16 +0200

biobambam2 (2.0.116-1) unstable; urgency=medium

  * avoid copying inline EOF blocks in bamfastcat

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 19 Jul 2019 12:27:19 +0200

biobambam2 (2.0.115-1) unstable; urgency=medium

  * add bamfiltereofblocks

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 19 Jul 2019 11:23:28 +0200

biobambam2 (2.0.114-1) unstable; urgency=medium

  * bump libmaus2 version

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 18 Jul 2019 10:55:18 +0200

biobambam2 (2.0.113-1) unstable; urgency=medium

  * add bamfastsplit
  * add multi threaded processing in bamseqchksum
  * add building numerical index in bamrecompress
  * add vcfconcat

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 17 Jul 2019 16:19:52 +0200

biobambam2 (2.0.112-1) unstable; urgency=medium

  * fix bamrecompress for case of BAM input with inline empty BGZF blocks

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 16 Jul 2019 13:57:00 +0200

biobambam2 (2.0.111-1) unstable; urgency=medium

  * use readFileAsStream to read text header file in bamfastcat/bamreheader/bamreplacechecksums
  * add tryoq flag in bamseqchksum

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Tue, 16 Jul 2019 11:13:33 +0200

biobambam2 (2.0.110-1) unstable; urgency=medium

  * add headerchecksums option in bamseqchecksum

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 15 Jul 2019 15:59:56 +0200

biobambam2 (2.0.109-1) unstable; urgency=medium

  * refactor bamreheader/bamfastcat and add bamreplacechecksums

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Mon, 15 Jul 2019 13:52:29 +0200

biobambam2 (2.0.108-1) unstable; urgency=medium

  * add include and exclude options in normalisefasta
  * make sure we write an EOF block in bamfastexploderef even for "empty" output files
  * add bisect tool
  * add bamfastnumextract
  * add low recompression bam input/output variant of exploderef functionality
  * add populaterefcache program
  * add bamexploderef

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 12 Jul 2019 16:34:24 +0200

biobambam2 (2.0.107-1) unstable; urgency=medium

  * add OQ tags in fastqtobam2
  * add bamfastcat
  * allow multi threading input and output in bamsplit
  * replace usage of PosixRegex by std::regex
  * fixes after changes in libmaus2
  * remove lasToBAM
  * added gzip output in vcffilterinfo
  * added program vcfpatchcontigprepend (prepend string to all seq ids in a VCF file)
  * add vcffilterinfo program (filter out all INFO fields not in a given filter list from a VCF file)
  * add append option in bamreheader
  * add bamreheader
  * try to avoid stalling on blocked write queue in bamauxmerge2
  * refactor Reference class out of bamauxmerge2 and add cigar string rewriting in bamauxmerge2

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Wed, 03 Jul 2019 21:04:34 +0200

biobambam2 (2.0.106-1) unstable; urgency=medium

  * fix email address in version bumping script

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Fri, 14 Jun 2019 12:39:35 +0200

biobambam2 (2.0.105-1) unstable; urgency=medium

  * no change version bump after fixing bumping script

 -- German Tischler <germant@miltenyibiotec.de>  Fri, 14 Jun 2019 12:11:44 +0200

biobambam2 (2.0.104-1) unstable; urgency=medium

  * version bumping script test

 -- German Tischler <germant@miltenyibiotec.de>  Fri, 14 Jun 2019 12:08:38 +0200

biobambam2 (2.0.103-1) unstable; urgency=low

  * bump libmaus2 version
  * remove bamparsort
  * fixes after libmaus2 changes
  * do not replace cigar string for unmapped reads in bamauxmerge2

 -- German Tischler-Höhle <germant@miltenyibiotec.de>  Thu, 14 Jun 2019 12:00:00 +0000
